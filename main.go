package main

import (
	"crypto/sha256"
	"fmt"
	"time"
)

const primary = 170141183460469127

// blockchain struct
type blockchain struct {
	chain               []block
	currentTransactions []Transaction
}

// block symbolize a block
type block struct {
	index        int
	timestamp    time.Time
	transactions []Transaction
	proof        int64
	previousHash [32]byte
}

func (b *blockchain) hash(block block) [32]byte {
	return sha256.Sum256([]byte(fmt.Sprintf("%v", block)))
}

func (b *blockchain) lastblock() *block {
	return &b.chain[len(b.chain)-1]
}

func (b *blockchain) newTransaction(sender string, recipient string, amount int) int {
	transaction := Transaction{sender, recipient, amount}
	b.currentTransactions = append(b.currentTransactions, transaction)
	return b.lastblock().index + 1
}

func (b *blockchain) newblock(proof int64, previousHash [32]byte) block {
	if previousHash == [32]byte{} {
		previousHash = b.hash(*b.lastblock())
	}

	block := block{
		len(b.chain) + 1,
		time.Now(),
		b.currentTransactions,
		proof,
		previousHash,
	}

	b.currentTransactions = []Transaction{}

	b.chain = append(b.chain, block)

	return block
}

func (b *blockchain) proofOfWork(lastProof int64) int64 {
	proof := int64(100)
	for !b.validProof(proof, lastProof) {
		proof++
	}

	fmt.Printf("Valid proof found: %v \n", proof)

	return proof
}

func compute(p1, p2 int64) int64 {
	return (p2 * p2 % primary) * p1
}

func (b *blockchain) validProof(proof int64, lastProof int64) bool {
	hashedMultiplication := sha256.Sum256([]byte(fmt.Sprintf("%v", compute(proof, lastProof))))
	return (hashedMultiplication[14] == 0 &&
		hashedMultiplication[15] == 0 &&
		hashedMultiplication[16] == 0)
	//	hashedMultiplication[24] == 0)
}

func (b blockchain) lastProof() int64 {
	return b.lastblock().proof
}

// New is the constructor for the blockchain
func New() *blockchain {
	b := &blockchain{}

	b.newblock(100, [32]byte{1})

	return b
}

func checkReplicate(arr []int64) bool {
	for i, x := range arr {
		for j, y := range arr {
			if x == y && j != i {
				fmt.Print(x, y)
				return true
			}
		}
	}

	return false
}

func main() {
	b := New()

	proofs := []int64{}

	proof := b.proofOfWork(b.lastProof())

	for i := 0; i < 10; i++ {
		b.newTransaction("Test", "test", i)
		proof = b.proofOfWork(proof)
		proofs = append(proofs, proof)
	}
	fmt.Printf("%v", checkReplicate(proofs))
	fmt.Printf("%v", b.chain)
}
