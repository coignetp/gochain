package main

// primary number used to a new proof calculation
const primary = 170141183460469127

// calculation of a number to hash
func compute(p1, p2 int64) int64 {
	return (p2 * p2 % primary) * p1
}