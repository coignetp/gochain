package main

import (
	"crypto/sha256"
	"fmt"
	"time"
)

// blockchain struct
type blockchain struct {
	chain               []block
	currentTransactions []Transaction
}

// hash function to call after proof calculation
func (b *blockchain) hash(block block) [32]byte {
	return sha256.Sum256([]byte(fmt.Sprintf("%v", block)))
}

// return the last block added in the blockchain
func (b *blockchain) lastblock() *block {
	return &b.chain[len(b.chain)-1]
}

// create a new transaction to add in the blockchain
func (b *blockchain) newTransaction(sender string, recipient string, amount int) int {
	transaction := Transaction{sender, recipient, amount}
	b.currentTransactions = append(b.currentTransactions, transaction)
	return b.lastblock().index + 1
}

// create a new block thanks to a proof
func (b *blockchain) newblock(proof int64, previousHash [32]byte) block {
	if previousHash == [32]byte{} {
		previousHash = b.hash(*b.lastblock())
	}

	block := block{
		len(b.chain) + 1,
		time.Now(),
		b.currentTransactions,
		proof,
		previousHash,
	}

	b.currentTransactions = []Transaction{}

	b.chain = append(b.chain, block)

	return block
}

// search and return the next valid proof of work
func (b *blockchain) proofOfWork(lastProof int64) int64 {
	proof := int64(100)
	for !b.validProof(proof, lastProof) {
		proof++
	}

	fmt.Printf("Valid proof found: %v \n", proof)

	return proof
}

// check if a proof is valid
func (b *blockchain) validProof(proof int64, lastProof int64) bool {
	hashedMultiplication := sha256.Sum256([]byte(fmt.Sprintf("%v", compute(proof, lastProof))))
	return (hashedMultiplication[14] == 0 &&
		hashedMultiplication[15] == 0 &&
		hashedMultiplication[16] == 0)
	//	hashedMultiplication[24] == 0)
}

// return the proof of the last block
func (b blockchain) lastProof() int64 {
	return b.lastblock().proof
}

// New is the constructor for the blockchain
func New() *blockchain {
	b := &blockchain{}

	b.newblock(100, [32]byte{1})

	return b
}