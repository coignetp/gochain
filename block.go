package main

import (
	"time"
)

// block symbolize a block
type block struct {
	index        int
	timestamp    time.Time
	transactions []Transaction
	proof        int64
	previousHash [32]byte
}