package main

// Transaction symbolize a transaction
type Transaction struct {
	sender    string
	recipient string
	amount    int
}